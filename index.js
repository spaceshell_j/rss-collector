/**
 * @desc A function to parse an XML rss feed and return a JSON object of
 * specified tag fields
 *
 * @example
 * Basic usage:
 *
 *  ```javascript
 * import RssParse from 'rss-parse'
 *
 * const rssParse = RssParse(data) //Sets XML data
 *  .itemSet('item') //Sets the feed items seperator
 *  .contentSet(['title', 'link']) //Sets the feed elements to return
 *  .clearCdata('title') //Clears any CDATA information for the selected element
 *  .linksSet('url=') //Image link query stored as an environment variable
 *  .contentGet() //returns parsed feed
 * ```
 *
 * @author James Nicholls | j-n@hotmail.co.uk
 * @param {(string|xml)} rss
 * @returns {module:rss-parse~RssParse}
 */

const RssParse = rss => {
  const expo = {}

  rss = String(rss)

  const clear = (key, cb) => {
    if(expo.contentData){
      expo.contentData = expo.contentData.map(clear => {
        if(!clear[key]) return clear
        return cb(clear)
      })
    }
    return expo
  }

  const clearEmptyObjects = arr => {
    return arr.filter(value => Object.keys(value).length !== 0)
  }

  /**
   * @desc Gets content of corresponding XML tags
   * @param {string} [query='item'] - XML tag name
   * @param {string} [text='rss'] - Input data
   * @param {boolean} [all='false'] - Weather to return all split items
   * @returns {array} Returns content within the XML tags matching
   * the input query
   */
  expo.item = (query = 'item', text = rss, all = false) => {
    let splitter = text.split(RegExp(`<${query}([^>]*|[\s\S]*?)>|<\/${query}>`))
    splitter = splitter.filter(empt => {
      return /\S/.test(empt) && empt !== undefined
    })
    splitter.pop()
    if((text.charAt(0) === splitter[0].charAt(0))) splitter.shift()
    return all ? splitter : splitter[0]
  }

  /**
   * @desc Sets content of corresponding XML tags
   * @param {string} [query='item'] - XML tag name
   * @param {string} [text='rss'] - Input data
   * @param {boolean} [all='true'] - Weather to return all split items
   * @returns {this}
   */
  expo.itemSet = (query = 'item', text = rss, all = true) => {
    expo.itemData = expo.item(query, text, all)
    return expo
  }

  /**
   * @desc Gets inner tags of an XML elements that are set
   * @param {string[]} titles - List of XML tags to collect
   * @returns {object[]} Returns an array of objects including the XML tags
   * as keys and the XML tag content for the values
   */
  expo.content = titles => {
    const contentData = (expo.itemData || expo.itemSet())
    return expo.itemData.map(itm => {
      return titles.reduce((arr, ttl, idx) => {
        return { ...arr, [ttl]: expo.item(ttl, itm) }
      }, {})
    })
  }

  /**
   * @desc Sets inner tags of an XML elements
   * @param {string[]} titles - List of XML tags to collect
   * @returns {this}
   */
  expo.contentSet = titles => {
    let updateContent = expo.content(titles)
    if(expo.contentData){
      expo.contentData = expo.contentData.map((obj, idx) => {
        return {...obj, ...updateContent[idx]}
      })
    } else {
      expo.contentData = updateContent
    }
    return expo
  }

  /**
   * @desc Gets quoted elements matching the query string
   * @param {string} [query='://'] - List of XML tags to collect
   * @returns {string[]} Returns an array links found matching the query
   */
  expo.links = (query = '://') => {
    return expo.itemData.map(link => {
      return link.split('"').filter(lnk => {
        return lnk.includes(query)
      })
    })
  }

  /**
   * @desc Sets quoted elements matching the query string
   * @param {string} [query='://'] - List of XML tags to collect
   * @returns {this}
   */
  expo.linksSet = query => {
    expo.links(query).forEach((lnk, idx) => {
      expo.contentData[idx].links = [...(expo.contentData[idx].links || []), ...lnk]
    })
    return expo
  }

  /**
   * @desc Clears any `CDATA` information for a given set tag
   * @param {string} key - Set XML tag name
   * @returns {this}
   */
  expo.clearCdata = key => {
    clear(key, cData => {
      let chk = /<!\[CDATA\[(.*?)\]\]>/.exec(cData[key])
      chk = chk[1].trim()
      chk = chk ? chk : cData[key]
      return {
        ...cData,
        [key]: chk
      }
    })
    return expo
  }

  /**
   * @desc Trims any leading and trailing whitespace
   * @param {string} key - Set XML tag name
   * @returns {this}
   */
  expo.clearWhite = key => {
    clear(key, cData => {
      let chk = cData[key].trim()
      chk = chk ? chk : cData[key]
      return {
        ...cData,
        [key]: chk
      }
    })
    return expo
  }

  /**
   * @desc Clears any `HTML` information for a given set tag
   * @param {string} key - Set XML tag name
   * @returns {this}
   */
  expo.clearHtml = key => {
    clear(key, cData => {
      let chk = cData[key]
        .replace(/(<br>|<\/p>)/g, '\n')
        .replace(/(<\/?[^>]+(>|$)|\t)/g, '')
        .replace(/\.(\S)/g, '. $1')
        .replace(/\\"/g, '"')
        .replace(/\\'/g, `'`);
      chk = chk ? chk : cData[key]
      return {
        ...cData,
        [key]: chk
      }
    })
    return expo
  }

  /**
   * @desc Get set item data
   * @returns {string[]} - Returns an array of feed items set
   */
  expo.itemGet = () => {
    return expo.itemData ? expo.itemData : console.log('No Item Data...')
  }

  /**
   * @desc Get set item data
   * @returns {object[]} - Returns an array of json objects containg any
   * content items set
   */
  expo.contentGet = () => {
    return expo.contentData ? expo.contentData : console.log('No Content Data...')
  }

  return rss ? expo : console.log('No Data Input...')
}

export default RssParse
