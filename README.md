# rss-collector

A simple lightweight RSS feed parser. Parses XML data as a string into a JSON object.

[![pipeline status](https://gitlab.com/spaceshell_j/rss-collector/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/rss-collector/commits/master)

## Table of Contents

* [Requirements](#requirements)
* [Installation](#installation)
* [Usage](#usage)
* [Properties](#Properties)
* [Methods](#Methods)
* [Testing](#Testing)
* [Authors](#Authors)

## Requirements
* Node `^5.0.0`
* NPM `^3.0.0`

## Installation

To install, you can use [npm](https://npmjs.org/)

```bash
$ npm install rss-collector
```

## Usage

After completing the [installation](#installation) step, you're ready to start the project!

Example:

```JavaScript
const rssParse = RssParse(data) //Sets XML data
  .itemSet('item') //Sets the feed items seperator
  .contentSet(['title', 'link']) //Sets the feed elements to return
  .clearCdata('title') //Clears any CDATA information for the selected element
  .linksSet('url=') //Image link query stored as an environment variable
  .contentGet() //returns parsed feed
```

> `itemSet()` has a default value of `'item'` and can be omitted

### Properties

  |Key|Description|
  |-|-|
  |rss| RSS feed in text format |

### Methods

`Set` and `clear` methods are used as part of the method chaining. `get` methods will return the parsed feed.


#### itemSet

Gets all of the data within the RSS items tag

```JavaScript
const rssParse = RssParse(data)
  .itemSet('item')
```

#### contentSet

Gets the content of RSS tags matching the values entered

```JavaScript
const rssParse = RssParse(data)
  .contentSet(['title', 'link'])
```

#### clearCdata

Clears the `CDATA` HTML element from any given content field

```JavaScript
const rssParse = RssParse(data)
  .contentSet(['content']) //Required
  .clearCdata('content')
```

#### linksSet

Returns any data within speech marks after the matching value entered

```JavaScript
const rssParse = RssParse(data)
  .contentSet(['content']) //Required
  .linksSet('url=')
```

#### contentGet

Sub description

```JavaScript
const rssParse = RssParse(data)
  .contentSet(['content']) //Required
  .contentGet()
```

## Testing

```bash
$ npm run test
```

## Authors

*James Nicholls*
